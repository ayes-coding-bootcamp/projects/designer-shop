function csvToJSON(csv) {
	let lines = csv.split("\n");
	let result = [];
	let headers;
	headers = lines[0].split(",");

	for (let i = 1; i < lines.length; i++) {
		let obj = {};

		if (lines[i] === undefined || lines[i].trim() === "") {
			continue;
		}

		let words = lines[i].split(",");
		for (let j = 0; j < words.length; j++) {
			obj[headers[j].trim()] = words[j];
		}

		result.push(obj);
	}
	return result;
}

let shopItems = [{
	"productTitle": "null",
	"company": "null",
	"price": 0,
	"productImage": "null",
	"shopLink": "null"
}]

shopItems = csvToJSON('productTitle,company,price,productImage,shopLink\n' +
	'Kanken Art Laptop 15 Special Edition Backpack,Fjallraven,$119.00,1.png,https://amzn.to/32MmHgL\n' +
	'Swing Arm Wood Desk Lamp,Tomons,$36.00,d2.png,https://amzn.to/2WX7vtD\n' +
	'Hajo Backpack,Ucon Acrobatics,$89.00,u6.png,https://amzn.to/2I3WvXM\n' +
	'Zed Bamboo Longboard,Retrospec,$60.00,u10.png,https://amzn.to/2YX3043\n' +
	'Electric Pour-Over Kettle,Fellow,$149.00,u9.png,https://amzn.to/2KfP3ee\n' +
	'Woodie Vintage Car,Candylab Toys,$35.00,h17.png,https://amzn.to/2FYB3zT\n' +
	'Logo Modernism,Taschen,$60.00,c3.png,https://www.bookdepository.com/Logo-Modernism-Jens-Muller-R-Roger-Remington/9783836545303/?a_aid=1991\n' +
	'Dieter Rams: As Little Design as Possible,Sophie Lovell,$150.00,d7.png,https://amzn.to/2YQLnCV\n' +
	'The Eye,Nathan Williams,$28.00,c6.png,https://amzn.to/2I3lbQ7\n' +
	'Gather Desk Organizer,Ugmonk,$149.00,h13.png,https://amzn.to/2IifSvA\n' +
	'Dipped Canteen,Corkcicle.,$28.00,h8.png,https://amzn.to/2G84CiT\n' +
	'Sayl Office Chair White,Herman Miller,$599.00,h2.png,https://amzn.to/2YWPz3N\n' +
	'Three Cherry Wood Notebook,Field Notes,$13.00,d5.png,https://amzn.to/2KhwTsz\n' +
	'From Japan,Counter Print,$16.00,c5.png,https://www.bookdepository.com/From-Japan/9780957081659?a_aid=1991\n' +
	'Arigato Desk Lamp,Grupa,$380.00,h6.png,https://www.grupaproducts.com/arigato/\n' +
	'Winston Regal Watch,Komono,$58.00,u7.png,https://amzn.to/2CZiPxO\n' +
	'Letter H Poster,Hey Studio,$45.00,d4.png,https://heyshop.es/collections/home/products/h-hey\n' +
	'Coffee Can,Blue Bottle Company,$11.00,u11.png,https://amzn.to/2UGLHEQ\n' +
	'Beoplay H4,Bang & Olufsen,$160.00,h9.png,https://amzn.to/2UFjEWp\n' +
	'Make It Now,Anthony Burrill,$22.00,p2.png,https://amzn.to/2OTHEQg\n' +
	'Black Side Chair,Eames Style,$54.00,h4.png,https://amzn.to/2UFHfGt\n' +
	'Min: The New Simplicity in Graphic Design,Thames & Hudson,$28.00,c22.png,https://amzn.to/2Ij1IKA\n' +
	'Grid systems in graphic design,Josef Müller-Brockmann,$42.00,c1.png,https://amzn.to/2UxtLww\n' +
	'Card Wallet,Herschel,$15.00,u12.png,https://amzn.to/2I5vxiq\n' +
	'Classic Advice Print Black,Good Fucking Design Advice,$30.00,p3.png,https://goodfuckingdesignadvice.com/products/classic-advice-print\n' +
	'Magic Mouse 2,Apple,$92.00,life-15.png,https://amzn.to/2D3yZWX\n' +
	'Travel Tumbler,Kinto,$34.00,life-17.png,https://amzn.to/2WTbsPL\n' +
	'Magic Keyboard,Apple,$139.00,life-16.png,https://amzn.to/2GbCf4A\n' +
	'Bottle Grinder,Menu,$75.00,u8.png,https://amzn.to/2WMJbdy\n' +
	'Geo Thermos,Normann Copenhagen,$62.00,home-8.png,https://amzn.to/2JLtxvk\n' +
	'Womb Chair & Ottoman,Kardiel,$799.00,home-10.png,https://amzn.to/2YdSBPy\n' +
	'Water Filter Glass Carafe,Soma,$30.00,home-9.png,https://amzn.to/2LCEfGP');

function initializeShop() {
	const template = document.querySelector("template");
	for (const current of shopItems) {
		let clone = template.content.cloneNode(true);

		clone.querySelector(".shop-link").href = current.shopLink;
		clone.querySelector(".product-image").src = "assets/images/" + current.productImage;
		clone.querySelector(".product-title").textContent = current.productTitle;
		clone.querySelector(".product-company").textContent = current.company;
		clone.querySelector(".product-prize").textContent = current.price;

		document.querySelector("main > section").appendChild(clone);
	}
}

initializeShop()